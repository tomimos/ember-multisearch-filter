import Component from '@ember/component';
import layout from '../templates/components/grid-items-counter';

export default Component.extend({
  layout,
  tagName: 'div',
  classNames: null,
  preText: 'Showing',
  postText: 'Results',
  currentSize: 0,
  totalSize: 0,
  init() {
    this._super(...arguments);
    this.set('classNames', ['two-col-select-results']);
  },
});
