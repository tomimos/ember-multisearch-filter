import layout from '../templates/components/input-trim';
import TextField from '@ember/component/text-field';
import strtrim from '../utils/strtrim';

export default TextField.extend({
  layout,
  type: 'text',
  inputKeyCode: null,
  blockSpaceKey: false,

  focusOut() {
    this.trim();
  },

  click() {
    this.set('inputKeyCode', null);
  },

  keyDown(e) {
    let inputKeyCode = this.get('inputKeyCode');
    let blockSpaceKey = this.get('blockSpaceKey');

    if (blockSpaceKey || (e.keyCode === 32 && inputKeyCode === 32)) {
      return false;
    }

    this.set('inputKeyCode', e.keyCode);
    this.setLastKeyUpValue(e.keyCode);

  },

  focusIn() {
    let value = this.get('value');

    // prevent a space character for empty strings
    if (!value || (value && value.length === 0)) {
      this.set('inputKeyCode', 32);
    } else {
      this.set('inputKeyCode', null);
    }
  },

  trim() {
    this.set('value', strtrim(this.get('value')));
  },

  willDestroyElement() {
    this._super(...arguments);
    this.set('inputKeyCode', null);
  },

  actions: {
    setLastKeyUpValue: function(keyValue){
      this.setLastKeyUpValue(keyValue);
    }

  }

});
