import Component from '@ember/component';
import layout from '../templates/components/multisearch-filter-dropdown-item';

export default Component.extend({
  layout,
  tagName: '',
  actions: {
    check(){
      this.set('item.checked', !this.get('item.checked'));
    }
  }
});
