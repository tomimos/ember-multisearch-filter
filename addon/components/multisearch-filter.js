import layout from '../templates/components/multisearch-filter';
import Component from '@ember/component';
import { task } from 'ember-concurrency';
import { observer } from '@ember/object';
import moment from 'moment';
import { A } from '@ember/array';
import { storageFor } from 'ember-local-storage';
import { getOwner } from '@ember/application';
import { runTask, pollTask, cancelPoll, runDisposables } from 'ember-lifeline';

export default Component.extend({
  init() {
    this._super(...arguments);
    this._pollToken = pollTask(this, 'keyUpWatcher');
  },

  layout,
  targetFilterValue: null,
  lastKeyUpTime: 0,
  lastKeyUpValue: null,
  keyUpDelay: 1250, // delay in ms
  keyUpInterval: 250, // interval for keyUpWatcher
  characterLimit: 3,
  enterKeyValue: 13,
  filterIconPosition: 'left',
  columnSize: 2,
  canSetObjects: true,

  appearance: storageFor('multisearch'),

  visibleFiltersObserver: observer('filters.@each.checked', function() {
    let currentPath = getOwner(this).lookup('controller:application').currentPath;
    currentPath = currentPath.replace(/\./g, '_');

    this.get('appearance').set(currentPath, this.get('filters').map(f => f.checked));
  }),

  keyUpWatcher(next) {
    let searchValue = this.get('targetFilterValue');
    let lastKeyUpTime = this.get('lastKeyUpTime');
    let lastKeyUpValue = this.get('lastKeyUpValue');
    let now = (new Date()).getTime();

    if ((!searchValue || searchValue && searchValue.length >= this.get('characterLimit')) &&
        (lastKeyUpTime && (now - lastKeyUpTime) > this.get('keyUpDelay')) && (lastKeyUpValue !=
        searchValue) && (lastKeyUpValue != this.get("enterKeyValue"))) {
      this.set('lastKeyUpTime', 0);
      this.applyFilters();
    }

    runTask(this, next, this.get('keyUpInterval'));
  },

  objectsObserver: observer('searchObjectsIn', 'searchObjectsIn.[]', 'searchObjectsIn.@each', function() {
    this.applyFilters();
  }),

  filtersObserver: observer('filters.@each.checkboxValue', 'filters.@each.selectValue', 'filters.@each.dateValue', function() {
    this.applyFilters();
  }),

  multiPropertyPartialStringMatchTask: task(function * (objects, filters) {
    for (var i=0; i < filters.length; i++){
      var filter = filters.objectAt(i);

      if (filter.queryParam) {
        this.setQueryParam(filter.queryParam, filter.rawValue ? filter.getValue() : filter.getReadableValue());
      }

      if (filter.backEndFilter) {
        continue;
      }

      objects = yield objects.filter((obj) => {
        var v = obj.get(filter.get('searchTarget'));
        var searchValue = filter.getReadableValue();

        if (typeof searchValue === 'undefined' || searchValue === null ||
            (Array.isArray(searchValue) && searchValue.length === 0)) {
          return true;
        }

        if (typeof v === 'undefined' || v === null) {

          return false;
        }

        if (filter.type === 'text') {
          return this.filterByText(v, searchValue);
        }

        if (searchValue && filter.type === 'checkbox') {
          return this.filterByCheckbox(v);
        }

        if (filter.type === 'select') {
          return this.filterBySelect(v, searchValue);
        }

        if (Array.isArray(searchValue) && searchValue.length && filter.type === 'multiselect') {
          return this.filterByMultiselect(v, searchValue);
        }

        if (filter.type === 'date') {
          let operation = filter.hasOwnProperty('operation') ? filter.operation : '=';

          return this.filterByDate(v, searchValue, operation);
        }

        return true;
      });
    }

    if (objects) {
      this.set('searchObjectsOut', objects);

      if (this.get('canSetObjects')) {
        this.setFilteredObjects(objects);
        this.setItemCounts(objects.length, this.get('searchObjectsIn.length'));

      }
    }
  }).restartable(),

  filterByText(value, search) {
    return value.toLowerCase().indexOf(search.toLowerCase()) !== -1;
  },

  filterByCheckbox(value) {
    var isTruthy = false;

    if (typeof value === 'string' || Array.isArray(value)) {
      isTruthy = value.length > 0;
    } else if (typeof value === 'number') {
      isTruthy = value > 0;
    } else if (typeof value === 'boolean') {
      isTruthy = value === true;
    }

    return isTruthy;
  },

  filterBySelect(value, search) {
    return value === search;
  },

  filterByMultiselect(value, searches) {
    return searches.includes(value);
  },

  filterByDate(value, search, operation) {
    let date = !isNaN(value) ? moment.unix(value) : moment(value);

    if (operation === '<') {
      return moment(search).isAfter(date);
    } else if (operation === '>') {
      return moment(search).isBefore(date);
    }

    return moment(search).isSame(date);
  },

  applyFilters: function() {
    var objects = [];

    // only for front-end filters, not necessary for back-end filters
    if (this.get('canSetObjects')) {
      objects = this.get('searchObjectsIn').filter(obj => obj.get('isDeleted') !== true);
    }
    this.get('multiPropertyPartialStringMatchTask').cancelAll();
    this.get('multiPropertyPartialStringMatchTask').perform(objects, this.get('filters'));
  },

  willDestroyElement() {
    this._super(...arguments);

    // finish polling for keyUp events
    cancelPoll(this, 'keyUpWatcher');

    // clear filters
    this.send('clear', false);

    // remove objects from the search arrays
    this.set('searchObjectsIn', []);
    this.set('searchObjectsOut', []);

    // update controller with empty array
    if (this.get('canSetObjects')) {
      this.setFilteredObjects([]);
    }
  },

  willDestroy() {
    this._super(...arguments);

    runDisposables(this);
  },

  didReceiveAttrs() {
    this._super(...arguments);

    let queryParams = this.get('queryParams');

    if (!Array.isArray(queryParams)) {
      return;
    }

    if (queryParams) {
      this.retrieveQueryParams();
    }
  },

  didInsertElement() {
    this._super(...arguments);

    let currentPath = getOwner(this).lookup('controller:application').currentPath;
    currentPath = currentPath.replace(/\./g, '_');

    let storedFilterOptions = this.get('appearance.' + currentPath);

    if (storedFilterOptions) {
      storedFilterOptions.forEach((f, index) => {
        let filter = this.get('filters').objectAt(index);

        if (filter) {
          filter.set('checked', f);
        }
      });
    }

    this.applyFilters();
  },

  actions: {

    setLastKeyUpValue: function(keyValue){
      this.set('lastKeyUpValue',keyValue);
    },
    changeFilter: function(){
      this.applyFilters();
    },
    inputKeyUp: function(value) {
      this.set('targetFilterValue', value);
      this.set('lastKeyUpTime', (new Date()).getTime());
    },
    clear: function(apply) {
      this.get('filters').forEach(function(filter){
        let defaultValue = filter.defaultValue;

        if (filter.type === 'checkbox') {
          filter.set('checkboxValue', defaultValue ? defaultValue : false);
        } else if (filter.type === 'select') {
          filter.set('selectValue', defaultValue ? defaultValue : null);
        } else if (filter.type === 'multiselect') {
          filter.set('multiselectValue', defaultValue ? defaultValue : A([]));
        } else if (filter.type === 'date') {
          filter.set('dateValue', defaultValue ? defaultValue : null);
        } else {
          filter.set('searchVal', defaultValue ? defaultValue : null);
        }
      });

      if (apply) {
        this.applyFilters();
      }
    }
  }
});
