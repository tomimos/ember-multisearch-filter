import Component from '@ember/component';
import layout from '../templates/components/pikaday-clear-icon';

export default Component.extend({
  layout,
  tagName: 'div',
  classNames: null,
  classNameBindings: null,
  icon: 'fa-times',
  altClass: 'pikaday-clear-icon',
  init() {
    this._super(...arguments);

    this.set('classNames', ['fa']);
    this.set('classNameBindings', ['icon', 'altClass']);
  },
  click() {
    this.set('targetDate', null);
    this.sendAction('dateReset');
  }
});
