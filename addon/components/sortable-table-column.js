import Component from '@ember/component';
import layout from '../templates/components/sortable-table-column';

export default Component.extend({
  layout,
  tagName: 'td',
  classNameBindings: null,
  columnClass: 'sortable-table-column',
  desc: false,
  target: 'id',
  ascIcon: 'sort-asc',
  descIcon: 'sort-desc',
  singleIcon: 'sort',
  separateIcons: true,

  init() {
    this._super(...arguments);
    this.set('classNameBindings', ['columnClass']);
  },
  click() {
    let targets = this.get('target').split(',');

    if (this.get('desc')) {
      targets = targets.map(t => t + ':desc');
    }

    this.sort(targets);
    this.toggleProperty('desc');
  }
});
