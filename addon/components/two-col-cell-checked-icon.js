import Component from '@ember/component';
import layout from '../templates/components/two-col-cell-checked-icon';

export default Component.extend({
  layout,
  tagName: ''
});
