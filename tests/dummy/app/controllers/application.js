import Controller from '@ember/controller';
import MultisearchFilterMixin from '../mixins/multisearch-filter-mixin';
import SortableTableColumnMixin from '../mixins/sortable-table-column-mixin';
import { A } from '@ember/array';

export default Controller.extend(MultisearchFilterMixin,SortableTableColumnMixin,{
    filterObjectsTarget: 'application',
    searchFilters: null,
    sortBy: null,
  init() {
    this._super(...arguments);

    var filters = [
      {
        searchTarget: 'firstName',
        searchPlaceHolder: 'First Name',
        checked: false
      },
      ,
      {
        searchTarget: 'disabled',
        searchPlaceHolder: 'Disabled',
        type: 'checkbox',
        columnSize: 3,
        checkboxValue: true,
        checked: false
      }
    ];

    this.setSearchFilters(filters);
    this.set('sortBy', A(['name']));

  }

});
