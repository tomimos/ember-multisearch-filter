import { A } from '@ember/array';
import Mixin from '@ember/object/mixin';
import FilterObject from '../utils/filter-object';

export default Mixin.create({
  currentSize: 0,
  totalSize: 0,

  setSearchFilters(searchFilters) {
    var filters = A();

    searchFilters.forEach(searchFilter => {
      var filter = FilterObject.create();
      filter.loadFilter(searchFilter);
      filters.addObject(filter);
    });

    this.set('searchFilters', filters);
  },

  actions: {
    setFilteredObjects: function(filteredObjects){
      this.set(this.get('filterObjectsTarget'), filteredObjects);
    },

    setItemCounts: function(currentSize, totalSize) {
      this.set('currentSize', currentSize);
      this.set('totalSize', totalSize);
    },

    setQueryParam: function(paramName, paramValue) {
      this.set(paramName, paramValue);
    },

    retrieveQueryParams: function() {
      this.get('searchFilters').forEach(filter => {
        let queryParam = filter.get('queryParam');

        if (queryParam) {
          let queryValue = this.get(queryParam);

          if (queryValue) {
            filter.setValue(queryValue);
          }
        }
      });
    }
  }
});
