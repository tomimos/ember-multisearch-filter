import { A } from '@ember/array';
import EmberObject, { computed } from '@ember/object';

export default EmberObject.extend({
  init: function(){
    this._super();
    this.setProperties({
      firstName: null,
      lastName: null,
      id: null,
      disabled: false
    });
  }
});
